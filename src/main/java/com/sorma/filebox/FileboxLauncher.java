package com.sorma.filebox;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.controlsfx.control.StatusBar;

import com.beust.jcommander.JCommander;
import com.sorma.filebox.view.FileboxController;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class FileboxLauncher extends Application{

	public static void main(String[] args) {
		RunCliParams parameters = new RunCliParams();
		JCommander parser = new JCommander(parameters);
		parser.setProgramName("com.sorma.filebox");
		parser.parse(args);

		if(parameters.isHelp()){
			parser.usage();
		}else if(parameters.getJustFail()){
			System.exit(1);
		}else if (parameters.getJustOk()) {
			System.exit(0);
		} else {
			launch(args);
		}

	}

	private static Logger logger = Logger.getLogger(FileboxApp.class);

	private Stage primaryStage;
	private  BorderPane rootPane;
	private FileboxApp app ;


	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage=primaryStage;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("view/Filebox.fxml"));
		try{
			rootPane=(BorderPane) loader.load();



			FileboxController controller = loader.getController();

			app = new FileboxApp(primaryStage, rootPane, controller);

			controller.setApplication(app);

			Scene scene = new Scene(rootPane);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Filebox");



			StatusBar statusBar = new StatusBar();
			controller.setStatusBar(statusBar);
			rootPane.setBottom(statusBar);


			primaryStage.show();


			app.viewManager.showView("MainPane");
			app.viewManager.sendMessage("Ready");

		}catch(IOException e){
			logger.error("Error loading application.",e);
		}
	}

	@Override
	public void stop() throws Exception {
		super.stop();
		app.close();
	}

}
