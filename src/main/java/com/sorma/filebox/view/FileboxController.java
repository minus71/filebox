package com.sorma.filebox.view;

import org.controlsfx.control.StatusBar;

import com.sorma.filebox.FileboxApp;

import javafx.fxml.FXML;

public class FileboxController {

	private StatusBar statusBar;

	@FXML
	public void close(){
		app.close();
	}

	private FileboxApp app;

	public void setApplication(FileboxApp app) {
		this.app=app;
	}

	public StatusBar getStatusBar() {
		return statusBar;
	}

	public void setStatusBar(StatusBar statusBar) {
		this.statusBar = statusBar;
	}


}
