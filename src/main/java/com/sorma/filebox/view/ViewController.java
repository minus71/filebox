package com.sorma.filebox.view;

import java.net.URL;
import java.util.ResourceBundle;

import com.sorma.filebox.FileboxApp;

import javafx.fxml.Initializable;

public abstract class ViewController implements Initializable{
	private FileboxApp application;
	private URL location;
	private ResourceBundle resources;

	public FileboxApp getApplication() {
		return application;
	}

	public void setApplication(FileboxApp application) {
		this.application = application;
	}

	public void postBuild(){

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.location=location;
		this.resources=resources;
		postBuild();
	}

	public URL getLocation() {
		return location;
	}

	public ResourceBundle getResources() {
		return resources;
	}

}
