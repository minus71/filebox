package com.sorma.filebox.view;

import static com.sorma.filebox.FXHelpers.*;
import java.io.File;
import java.util.regex.Pattern;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;

public class MainPaneController extends ViewController {

	@FXML
	private Button chooseDirButton;


	@FXML
	private Label rootPathLabel;


	@FXML
	private TextField filterField;

	@FXML
	private ListView<File> filesView;

	@FXML
	private Spinner<Integer> depthSpinner;

	private IntegerProperty maxDepth = new SimpleIntegerProperty(3);

	private ObjectProperty<File> fileChosenProperty = new SimpleObjectProperty<File>(new File("."));
	private StringProperty filter;

	private ObservableList<File> filteredFiles = FXCollections.observableArrayList();



	@Override
	public void postBuild() {

//
//		SpinnerValueFactory<Integer> vf = new SpinnerValueFactory<Integer>(){
//
//			@Override
//			public void decrement(int steps) {
//				maxDepth.set(maxDepth.get()-steps);
//			}
//
//			@Override
//			public void increment(int steps) {
//				maxDepth.set(maxDepth.get()+steps);
//			}
//
//		};
//
		SpinnerValueFactory.IntegerSpinnerValueFactory vf = new SpinnerValueFactory.IntegerSpinnerValueFactory(1,100);
		depthSpinner.setValueFactory(vf);


		maxDepth.bind(vf.valueProperty());
		maxDepth.addListener((obj,past,current)->{
			if(hasCurrentDir()){
				getApplication().getFiles(fileChosenProperty.get(),getPattern(),filteredFiles,maxDepth.get());
			}
		});

		rootPathLabel.textProperty().bind(fileChosenProperty.asString());
		filter=new SimpleStringProperty(".+");

		filterField.textProperty().bindBidirectional(filter);

		chooseDirButton.setOnAction(
			evt -> {
				DirectoryChooser chooser = new DirectoryChooser();
				if(hasCurrentDir()){
					chooser.setInitialDirectory(fileChosenProperty.get());
				}
				File fileChosen =  chooser.showDialog(getApplication().primaryStage);
				fileChosenProperty.set(fileChosen);
			}
		);

		fileChosenProperty.addListener((obs,oldVal,newVal)->{
			getApplication().getFiles(newVal,getPattern(),filteredFiles,maxDepth.get());
		});

		filterField.textProperty().addListener((obs,oldVal,newVal)->{
			try{
				Pattern newPattern = Pattern.compile(newVal);
				removeStyleClass("val_fail", filterField.getStyleClass());
				if(hasCurrentDir()){
					getApplication().getFiles(fileChosenProperty.get(),getPattern(),filteredFiles,maxDepth.get());
				}
			}catch(Exception e){
				addStyleClass("val_fail", filterField.getStyleClass());
			}
		});



		filesView.setItems(filteredFiles);


	}

	private boolean hasCurrentDir() {
		return fileChosenProperty.isNotNull().get();
	}

	private Pattern getPattern() {
		String value = filter.getValue();
		Pattern rx ;
		try{
			rx = Pattern.compile(value);
		}catch(Exception e){
			rx=null;
		}
		return rx;
	}

}
