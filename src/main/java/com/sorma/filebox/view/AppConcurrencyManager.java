package com.sorma.filebox.view;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class AppConcurrencyManager {
	private ScheduledExecutorService pool;

	private AtomicInteger counter = new AtomicInteger(0);

	public AppConcurrencyManager(int numThread) {
		pool = Executors.newScheduledThreadPool(numThread);
	}

	public <R> Future<R> submit(Supplier<R> fun) {
		long delay = 0;
		Callable<R> callable = () -> {

			return fun.get();
		};

		TimeUnit unit = TimeUnit.MILLISECONDS;
		ScheduledFuture<R> task = pool.schedule(callable, delay, unit);

		return task;
	}

	public void submit(Callable<Void> fun) {
		long delay = 0;
		TimeUnit unit = TimeUnit.MILLISECONDS;
		pool.schedule(fun, delay, unit);
	}

	public boolean stop() {
		try {
			System.out.println("Stopping executors");
			pool.shutdownNow();
			pool.awaitTermination(10, TimeUnit.SECONDS);
			return true;
		} catch (InterruptedException e) {
			System.out.println("Failed stopping executors");
			return pool.isTerminated();
		}
	}

}
