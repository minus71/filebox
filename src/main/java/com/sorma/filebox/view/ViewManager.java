package com.sorma.filebox.view;

import java.io.IOException;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.controlsfx.control.StatusBar;

import com.sorma.filebox.FileboxApp;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

public class ViewManager {
	private Logger logger = Logger.getLogger(ViewManager.class);

	public final FileboxApp app;
	private BorderPane container;

	private Queue<AppView> views;
	private Map<String,AppView> viewsMap;

	public ViewManager(FileboxApp app) {
		super();
		this.app = app;
		container = app.rootPane;
		views = new ConcurrentLinkedQueue<AppView>();
		viewsMap = new ConcurrentHashMap<String,AppView>();
	}


	public void showView(String name){
		AppView appView = viewsMap.get(name);
		if(appView!=null){
			views.remove(appView);
		}else{
			try {
				appView = buildView(name);
				viewsMap.put(name, appView);
				appView.getView();
			} catch (IOException e) {
				error("Error loading view:"+name,e);
			}
		}
		if(appView!=null){
			views.add(appView);
			container.setCenter(appView.getView());
		}
	}

	public void closeView(String name){
		AppView appView = viewsMap.get(name);
		if(appView!=null){
			views.remove(appView);
			viewsMap.remove(name);
			AppView olderView = views.peek();
			container.setCenter(olderView.getView());
		}
	}


	private void error(String string, Exception e) {
		logger.error(string,e);
	}


	protected AppView buildView(String name) throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(""
				+ name
				+ ".fxml"));
		Pane view=(Pane) loader.load();
		ViewController controller =  loader.getController();
		controller.setApplication(app);
		AppView appView = new AppView(name, view, controller);

		return appView;
	}


	public void sendMessage(String string) {
		Platform.runLater(()->{
			getStatusBar().setText(string);
		});
	}


	private StatusBar getStatusBar() {
		return app.rootPaneController.getStatusBar();
	}



}
