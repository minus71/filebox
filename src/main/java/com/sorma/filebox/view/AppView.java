package com.sorma.filebox.view;

import javafx.scene.layout.Pane;

public class AppView {

	private Pane view;
	private ViewController controller;
	private String name;

	public AppView(String name, Pane view, ViewController controller) {
		super();
		this.name = name;
		this.view = view;
		this.controller = controller;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppView other = (AppView) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Pane getView() {
		return view;
	}

	public ViewController getController() {
		return controller;
	}

	public String getName() {
		return name;
	}


}
