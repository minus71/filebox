package com.sorma.filebox;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.sorma.filebox.view.AppConcurrencyManager;
import com.sorma.filebox.view.FileboxController;
import com.sorma.filebox.view.ViewManager;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class FileboxApp {

	public final Stage primaryStage;
	public final BorderPane rootPane;
	public final ViewManager viewManager;
	public final FileboxController rootPaneController;
	public final AppConcurrencyManager concurrencyManager;

	private Logger logger = Logger.getLogger(FileboxApp.class);

	public FileboxApp(Stage primaryStage, BorderPane rootPane, FileboxController controller) {
		this.primaryStage = primaryStage;
		this.rootPane = rootPane;
		rootPaneController = controller;


		viewManager = new ViewManager(this);
		concurrencyManager = new AppConcurrencyManager(4);

	}

	public void close() {
		Platform.exit();
		if(!concurrencyManager.stop()){
			System.exit(1);
		}
	}

	private Future<Integer> currentRequest;

	public synchronized void getFiles(File newVal, Pattern rx, final ObservableList<File> filesObs, Integer maxDepth) {
		if(currentRequest!=null){
			if(!currentRequest.isDone()){
				currentRequest.cancel(true);
				viewManager.sendMessage("Previous task cancelled");
			}
		}

		viewManager.sendMessage("Exploring "+newVal);

		currentRequest = concurrencyManager.submit(() -> {
			if(_getFiles(newVal, rx, filesObs, maxDepth)){
				viewManager.sendMessage("Completed: ("+ filesObs.size()
						+ ")");
			}

			return 0;
		});

	}

	private boolean _getFiles(File newVal, Pattern rx, ObservableList<File> filesObs, int maxDepth) {
		if(Thread.currentThread().isInterrupted()){
			return false;
		}
		AtomicBoolean complete= new AtomicBoolean(true);
		try {
			Platform.runLater(()->filesObs.clear());

			Files.walkFileTree(newVal.toPath(),Collections.EMPTY_SET ,maxDepth,new SimpleFileVisitor<Path>  () {
				List<File> batch = new ArrayList<>();
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					if(onInterrupt()){
						logger.info("Killing search");
						return FileVisitResult.TERMINATE;
					}
					return super.preVisitDirectory(dir, attrs);
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					if(onInterrupt()){
						logger.info("Killing search");
						return FileVisitResult.TERMINATE;
					}
					File f = file.toFile();
					if(rx.matcher(f.getName()).find()){
						batch.add(f);
					}
					if(batch.size()>10){
						final List<File> committed = batch;
						batch = new ArrayList<>();
						Platform.runLater(()->{
							filesObs.addAll(committed);
						});
					}

					return super.visitFile(file, attrs);
				}

				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					final List<File> committed = batch;
					batch = new ArrayList<>();
					Platform.runLater(()->{
						filesObs.addAll(committed);
					});
					return super.postVisitDirectory(dir, exc);
				};

				public Boolean onInterrupt(){
					if(Thread.currentThread().isInterrupted()){
						complete.set(false);
						return true;
					}
					return false;
				}
			} );

		} catch (IOException e) {
			logger.error("Error reading dir: "+newVal,e);
			return false;
		}
		logger.info("Exiting visit");

		return complete.get();
	}

	private void error(String string, Throwable e) {
		logger.error(string,e);
	}

}
