package com.sorma.filebox;

import com.beust.jcommander.Parameter;

public class RunCliParams {

	@Parameter(names = "--help", help = true)
	private boolean help;

	@Parameter(names = { "--fail" }, description = "Just fail with exit code 1")
	private Boolean justFail = false;

	@Parameter(names = { "--ping" }, description = "Just exit with code 0")
	private Boolean justOk = false;

	@Parameter(names = { "--echo" }, description = "Echoes main arg to stdout")
	private String echo;

	public Boolean getJustFail() {
		return justFail;
	}

	public void setJustFail(Boolean justFail) {
		this.justFail = justFail;
	}

	public Boolean getJustOk() {
		return justOk;
	}

	public void setJustOk(Boolean justOk) {
		this.justOk = justOk;
	}

	public String getEcho() {
		return echo;
	}

	public void setEcho(String echo) {
		this.echo = echo;
	}

	public boolean isHelp() {
		return help;
	}

	public void setHelp(boolean help) {
		this.help = help;
	}

}
