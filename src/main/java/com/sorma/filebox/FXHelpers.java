package com.sorma.filebox;

import java.util.Observable;

import javafx.collections.ObservableList;

public class FXHelpers {

	public static boolean addStyleClass(String clazz, ObservableList<String> currentClasses ){
		if(!currentClasses.contains(clazz)){
			currentClasses.add(clazz);
			return true;
		}else{
			return false;
		}

	}

	public static boolean removeStyleClass(String clazz, ObservableList<String> currentClasses ){
		if(currentClasses.contains(clazz)){
			currentClasses.remove(clazz);
			return true;
		}else{
			return false;
		}
	}
}
